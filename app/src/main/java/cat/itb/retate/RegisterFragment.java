package cat.itb.retate;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegisterFragment extends Fragment {

    private RegisterViewModel mViewModel;
    private EditText precioTabaco, cantidadFumada;
    private EditText usernameText2, passwordText2, repeatPasswordText, emailText;
    private TextInputLayout usernameText2Layout, passwordText2Layout, repeatPasswordTextLayout, emailTextLayout;
    private HomeViewModel homeViewModel;
    FirebaseAuth fAuth;
    ProgressDialog dialog;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate( R.layout.register_fragment, container, false );
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        usernameText2 = view.findViewById(R.id.usernameText2 );
        passwordText2 = view.findViewById(R.id.passwordText2);
        repeatPasswordText = view.findViewById(R.id.repeatPasswordText);
        emailText = view.findViewById(R.id.emailText);

        usernameText2Layout = view.findViewById(R.id.username2TextLayout );
        passwordText2Layout = view.findViewById(R.id.password2TextLayout );
        repeatPasswordTextLayout = view.findViewById(R.id.repeatPasswordTextLayout);
        emailTextLayout = view.findViewById(R.id.emailTextLayout);

        precioTabaco = view.findViewById(R.id.precioTabacoText);
        cantidadFumada = view.findViewById(R.id.cantidadFumadaText);

        Button buttonLogin3 = view.findViewById(R.id.PlusOneBttn);
        buttonLogin3.setOnClickListener(this::Login);
        Button buttonRegister3 = view.findViewById(R.id.registerBtn3);
        buttonRegister3.setOnClickListener(this::Register);

        FirebaseApp.initializeApp(getContext());
        fAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated( savedInstanceState );
        mViewModel = ViewModelProviders.of( this ).get( RegisterViewModel.class );


    }
    private void loadingObserver(Boolean bool) {
        if (bool) {
            dialog = ProgressDialog.show(getContext(), "Loading", "...", true);
            dialog.show();
        } else {
            if(dialog!=null)
                dialog.dismiss();
        }

    }

    private void loggedObserve(Boolean logged) {
        if (logged) {
            Navigation.findNavController(getView()).navigate(R.id.action_register_to_login);
        }
    }

    private void errorObserve(String error) {
        if (error!=null) {
            Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

    private void Login(View view) {

        Navigation.findNavController(view).navigate(R.id.action_register_to_login);
    }

    private void Register(View view) {

        boolean validacio = true;

        if(usernameText2.getText().toString().isEmpty()) {
            validacio = false;
            usernameText2Layout.setError(getString(R.string.isEmpty));
            scrollTo(usernameText2Layout);
        }

        if(passwordText2.getText().toString().isEmpty()) {
            validacio = false;
            passwordText2Layout.setError(getString(R.string.isEmpty));
            scrollTo(passwordText2Layout);
        }

        if(repeatPasswordText.getText().toString().isEmpty()) {
            validacio = false;
            repeatPasswordTextLayout.setError(getString(R.string.isEmpty));
            scrollTo(repeatPasswordTextLayout);
        }

        if(!passwordText2.getText().toString().equals(repeatPasswordText.getText().toString())){
            validacio = false;
            repeatPasswordTextLayout.setError(getString(R.string.notSamePassword));
            scrollTo(repeatPasswordTextLayout);
        }

        if(emailText.getText().toString().isEmpty()) {
            validacio = false;
            emailTextLayout.setError(getString(R.string.isEmpty));
            scrollTo(emailTextLayout);
        }

        if (!isEmailValid(emailText.getText().toString())){
            validacio = false;
            emailTextLayout.setError(getString(R.string.notCorrect));
            scrollTo(emailTextLayout);
        }

        if (validacio) {

            String email = emailText.getText().toString().trim();
            String password = passwordText2.getText().toString().trim();

            fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(getContext(), "User Created.", Toast.LENGTH_SHORT).show();


                      //  homeViewModel.setCigsPerDay(Integer.parseInt(cantidadFumada.getText().toString()));
                      //  homeViewModel.setPriceTobacco(Double.parseDouble(precioTabaco.getText().toString()));

                        Navigation.findNavController( view ).navigate(R.id.action_register_to_login);


                    }else{
                        Toast.makeText(getContext(),"Error!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    private boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void scrollTo (TextInputLayout targetView){
        targetView.getParent().requestChildFocus(targetView, targetView);
    }
}
