package cat.itb.retate;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class HomeFragment extends Fragment {


    Random random = new Random();
    int randomNum1;
    private HomeViewModel mViewModel;
    private TextView quote;
    private TextView challenge;
    private TextView moneyWasted;
    private TextView currentCigs;
    private Button onePlus;
    private EditText limit;
    private double money;
    private int cigsPerDay;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        money = mViewModel.calculateTotal();
        cigsPerDay = mViewModel.getCigsPerDay();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        quote = view.findViewById(R.id.quote);
        challenge = view.findViewById(R.id.challenge);
        moneyWasted = view.findViewById(R.id.money);
        limit = view.findViewById(R.id.limitToSmoke);
        currentCigs = view.findViewById(R.id.smoked);
        onePlus = view.findViewById(R.id.PlusOneBttn);
        moneyWasted.setText(String.valueOf(money));
        limit.setText(String.valueOf(cigsPerDay));

        displayQuote();
        displayChallenge();
        onePlus.setOnClickListener(this::AddOneCig);
    }

    private void AddOneCig(View view) {

       int temp = Integer.parseInt(currentCigs.getText().toString()) + 1;
       String temp2 = ""+temp;
       currentCigs.setText(temp2);
    }

    private void displayQuote() {

        randomNum1 = random.nextInt((9+1)-1) + 1;
        switch (randomNum1) {
            case 1:
                quote.setText(R.string.quote1);
                break;
            case 2:
                quote.setText(R.string.quote2);
                break;
            case 3:
                quote.setText(R.string.quote3);
                break;
            case 4:
                quote.setText(R.string.quote4);
                break;
            case 5:
                quote.setText(R.string.quote5);
                break;
            case 6:
                quote.setText(R.string.quote6);
                break;
            case 7:
                quote.setText(R.string.quote7);
                break;
            case 8:
                quote.setText(R.string.quote8);
                break;
            case 9:
                quote.setText(R.string.quote9);
                break;
        }
    }

    private void displayChallenge(){

        int randomNum2 = random.nextInt((9+1)-1) + 1;
        switch (randomNum2) {
            case 1:
                challenge.setText(R.string.challenge1);
                break;
            case 2:
                challenge.setText(R.string.challenge2);
                break;
            case 3:
                challenge.setText(R.string.challenge3);
                break;
            case 4:
                challenge.setText(R.string.challenge4);
                break;
            case 5:
                challenge.setText(R.string.challenge5);
                break;
            case 6:
                challenge.setText(R.string.challenge6);
                break;
            case 7:
                challenge.setText(R.string.challenge7);
                break;
            case 8:
                challenge.setText(R.string.challenge8);
                break;
            case 9:
                challenge.setText(R.string.challenge9 );
                break;
        }
    }
}
