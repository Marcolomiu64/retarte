package cat.itb.retate;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import java.util.Random;

public class HomeViewModel extends AndroidViewModel {
    private double money = 0;
    private double priceTobacco;
    private int cigsPerDay = 10;
    private final int MONTH = 30;
    private final int STANDARD_AMOUNT = 20;

    public HomeViewModel(@NonNull Application application) {
        super(application);
    }

    public double getMoney() {
        return money;
    }


    public void setPriceTobacco(double priceTobacco) {
        this.priceTobacco = priceTobacco;
    }

    public double getPriceTobacco() {
        return priceTobacco;
    }

    public void setCigsPerDay(int cigsPerDay) {
        this.cigsPerDay = cigsPerDay;
    }

    public int getCigsPerDay() {
        return cigsPerDay;
    }

    public double calculateTotal(){
        money = (MONTH/(STANDARD_AMOUNT/cigsPerDay))*priceTobacco;
        return money;
    }
}

