package cat.itb.retate;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginFragment extends Fragment {

    private LoginViewModel mViewModel;
    EditText emailText, passwordText;
    TextInputLayout emailTextLayout, passwordTextLayout;
    Button buttonLogin2, buttonRegister2;
    ProgressDialog dialog;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate( R.layout.login_fragment, container, false );
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        emailText = getView().findViewById(R.id.emailText);
        emailTextLayout = getView().findViewById(R.id.emailTextLayout);
        passwordText = getView().findViewById(R.id.passwordText);
        passwordTextLayout = getView().findViewById(R.id.passwordTextLayout);

        buttonLogin2 = view.findViewById(R.id.loginBtn2);
        buttonRegister2 = view.findViewById(R.id.registerBtn2);
        buttonLogin2.setOnClickListener(this::Login);
        buttonRegister2.setOnClickListener(this::Register);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated( savedInstanceState );
        mViewModel = ViewModelProviders.of( this ).get( LoginViewModel.class );


    }

    private void loadingObserver(Boolean bool) {

        if (bool) {
            dialog = ProgressDialog.show(getContext(), "Loading", "...", true);
            dialog.show();
        } else {
            if(dialog!=null) dialog.dismiss();
        }
    }

    private void loggedObserve(Boolean logged) {
        if (logged) {
            Navigation.findNavController(getView()).navigate(R.id.action_login_to_home);
        }
    }

    private void errorObserve(String error) {
        if (error!=null) {
            Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void Login(View view) {

        boolean validacio = true;

        if(emailText.getText().toString().isEmpty()) {
            validacio = false;
            emailTextLayout.setError(getString(R.string.isEmpty));
            scrollTo(emailTextLayout);
        }

        if (!isEmailValid(emailText.getText().toString())){
            validacio = false;
            emailTextLayout.setError(getString(R.string.notCorrect));
            scrollTo(emailTextLayout);
        }

        if(passwordText.getText().toString().isEmpty()) {
            validacio = false;
            passwordTextLayout.setError(getString(R.string.isEmpty));
            scrollTo(passwordTextLayout);
        }
        if (validacio){
            Navigation.findNavController(view).navigate(R.id.action_login_to_home);
        }
    }

    private boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void Register(View view) {

        Navigation.findNavController(view).navigate(R.id.action_login_to_register);
    }

    private void scrollTo (TextInputLayout targetView){
        targetView.getParent().requestChildFocus(targetView, targetView);
    }
}
